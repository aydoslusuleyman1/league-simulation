<?php

namespace App\Domain\GameStatistics\Repository;

use App\Domain\GameStatistics\Model\GameStatistics;
use App\Domain\GameStatistics\Repository\Contracts\GameStatisticsRepositoryInterface;

class GameStatisticsRepository implements GameStatisticsRepositoryInterface
{
    private GameStatistics $model;

    /**
     * GameStatisticsRepository constructor.
     */
    public function __construct(GameStatistics $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function insertGameStatisticByTeam(array $data)
    {
        return $this->model->create($data);
    }

    public function checkGamePlayedByGameId(int $gameId): bool
    {
        return $this->model->newQuery()->where('game_id', $gameId)->count() > 0;
    }

    public function checkIsGamesPlayedByWeek(int $weekId): bool
    {
        return $this->model->newQuery()->whereHas('game', function ($query) use ($weekId) {
                return $query->where('week', '=', $weekId);
            })->count() > 0;
    }

    public function getLastPlayedGame()
    {
        return $this->model->newQuery()->orderByDesc('id')->first();
    }
}
