<?php

namespace App\Domain\Standing\Repository\Contacts;

interface StandingRepositoryInterface
{
    public function createInitialStanding(int $teamId);

    public function getStandings();

    public function getTeamStanding(int $teamId);

    public function updateStandingByTeamId(array $data);

    public function isFixtureArranged(): bool;
    public function removeAll(): void;
}
