<?php

namespace App\Domain\Schedule\Service;

use App\Domain\Game\Service\GameService;
use App\Domain\Standing\Service\StandingService;
use App\Domain\Team\Service\TeamService;

class ScheduleService
{
    private TeamService $teamService;

    private GameService $gameService;

    private StandingService $standingService;

    public function __construct(TeamService $teamService, GameService $gameService, StandingService $standingService)
    {
        $this->teamService = $teamService;
        $this->gameService = $gameService;
        $this->standingService = $standingService;
    }

    public function schedule()
    {
        $teams = $this->teamService->getAllTeams();
        $teamsInRound = $teams->toArray();

        shuffle($teamsInRound);
        $teamsCount = count($teamsInRound);

        if ($teamsCount === 0) {
            return;
        }

        if ($teamsCount % 2 == 1) {
            $teamsInRound[] = NULL;
            $teamsCount++;
        }

        $gamesCount = $teamsCount - 1;

        $home = [];
        $away = [];

        for ($i = 0; $i < $teamsCount / 2; $i++) {
            $home[$i] = $teamsInRound[$i]['id'];
            $away[$i] = $teamsInRound[$teamsCount - 1 - $i]['id'];
        }

        $firstHalfCalendar = [];
        for ($i = 0; $i < $gamesCount; $i++) {
            if (($i % 2) == 0) {
                for ($j = 0; $j < $teamsCount / 2; $j++) {
                    $firstHalfCalendar[$i][] = [$away[$j], $home[$j]];
                }
            } else {
                for ($j = 0; $j < $teamsCount / 2; $j++) {
                    $firstHalfCalendar[$i][] = [$home[$j], $away[$j]];
                }
            }

            $pivot = $home[0];
            array_unshift($away, $home[1]);
            $carryover = array_pop($away);
            array_shift($home);
            $home[] = $carryover;
            $home[0] = $pivot;
        }

        $result = self::reverseGames($firstHalfCalendar);

        foreach ($result as $weekIndex => $items){
            foreach ($items as $item){
                $this->gameService->saveGame($item[0], $item[1], $weekIndex + 1);
            }
        }

        $this->standingService->initStandings($teams->toArray());
    }

    public static function reverseGames(array $firstHalfCalendar): array
    {
        $secondHalfCalendar = [];

        foreach ($firstHalfCalendar as $weekKey => $weeks) {
            foreach ($weeks as $match => $week) {
                $secondHalfCalendar[$weekKey][$match] = [
                    $week[1],
                    $week[0],
                ];
            }
        }

        return array_merge($firstHalfCalendar, $secondHalfCalendar);
    }
}
