<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\GameStatistics\Service\GameStatisticsService;
use App\Domain\Standing\Helpers\StandingHelper;
use App\Domain\Standing\Service\StandingService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ConfigController extends Controller
{
    public function index(StandingService $standingService): JsonResponse
    {
        $isFixtureArranged = $standingService->isFixtureArranged();
        return response()->json(['isFixtureArranged' => $isFixtureArranged]);
    }

    public function simulation(GameStatisticsService $gameStatisticsService, int $weekId): JsonResponse
    {
        $isLastWeek = StandingHelper::isLastWeek($weekId);
        $isPreviousWeekPlayed  = $weekId == 1 || $gameStatisticsService->isWeeklyGamesPlayed($weekId - 1);
        $isCurrentWeekPlayed = $gameStatisticsService->isWeeklyGamesPlayed($weekId);
        $isNextWeekPlayed = $gameStatisticsService->isWeeklyGamesPlayed($weekId + 1);

        return response()->json([
            'isLastWeek' => $isLastWeek,
            'isCurrentWeekPlayed' => $isCurrentWeekPlayed,
            'isNextWeekPlayed' => $isNextWeekPlayed,
            'isPreviousWeekPlayed' => $isPreviousWeekPlayed
        ]);
    }
}
