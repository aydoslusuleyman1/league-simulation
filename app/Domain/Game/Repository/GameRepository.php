<?php

namespace App\Domain\Game\Repository;

use App\Domain\Game\Model\Game;
use App\Domain\Game\Repository\Contacts\GameRepositoryInterface;

class GameRepository implements GameRepositoryInterface
{
    private Game $model;

    /**
     * GameRepository constructor.
     */
    public function __construct(Game $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function saveGame(int $homeTeamId, int $awayTeamId, int $weekIndex)
    {
        return $this->model->create([
            'week' => $weekIndex,
            'home_team_id' => $homeTeamId,
            'away_team_id' => $awayTeamId
        ]);
    }

    public function getGames()
    {
        return $this->model->all();
    }

    public function getGamesByWeekId(int $weekId, bool $getFurther)
    {
        $operand = '=';
        if ($getFurther){
            $operand = '>=';
        }
        return $this->model->where('week', $operand, $weekId)->get();
    }

    public function getMaxWeekIndex(): int
    {
        return $this->model->newQuery()->max('week') ?? 0;
    }
}
