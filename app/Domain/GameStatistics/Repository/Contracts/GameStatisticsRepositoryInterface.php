<?php

namespace App\Domain\GameStatistics\Repository\Contracts;

interface GameStatisticsRepositoryInterface
{
    public function getAll();

    public function insertGameStatisticByTeam(array $data);

    public function checkGamePlayedByGameId(int $gameId): bool;

    public function checkIsGamesPlayedByWeek(int $weekId): bool;

    public function getLastPlayedGame();
}
