<?php

namespace App\Domain\Standing\Repository;

use App\Domain\Standing\Model\Standing;
use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;

class StandingRepository implements StandingRepositoryInterface
{
    private Standing $model;

    /**
     * StandingRepository constructor.
     */
    public function __construct(Standing $model)
    {
        $this->model = $model;
    }

    public function createInitialStanding(int $teamId)
    {
        $this->model->create([
            'team_id' => $teamId,
            'points' => 0,
            'goals_for' =>0,
            'goals_against' => 0,
            'win' => 0,
            'drawn' => 0,
            'lost' => 0
        ]);
    }

    public function getStandings()
    {
        return $this->model->newQuery()->orderBy('points','desc')->get();
    }

    public function getTeamStanding(int $teamId)
    {
        return $this->model->where('team_id', $teamId)->first();
    }

    public function updateStandingByTeamId(array $data)
    {
        $this->model->where('team_id' , '=', $data['team_id'])
            ->update([
                'points' => $data['points'],
                'goals_for' => $data['goals_for'],
                'goals_against' => $data['goals_against'],
                'win' => $data['win'],
                'drawn' => $data['drawn'],
                'lost' => $data['lost']
            ]);
    }

    public function isFixtureArranged(): bool
    {
        return $this->model->all()->count() > 0;
    }


    public function removeAll(): void
    {
        $this->model->newQuery()->truncate();
    }
}
