<?php

namespace App\Domain\Core\Enum;

final class TeamTypeEnum
{
    const HOME_TEAM = 1;
    const AWAY_TEAM = 2;
}
