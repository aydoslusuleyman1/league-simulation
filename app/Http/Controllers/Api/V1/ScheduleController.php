<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Core\Service\ResetService;
use App\Domain\Game\Service\GameService;
use App\Domain\Schedule\Service\ScheduleService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class ScheduleController extends Controller
{
    private ScheduleService $scheduleService;
    private GameService $gameService;
    private ResetService $resetService;

    public function __construct(ScheduleService $scheduleService, GameService $gameService, ResetService $resetService)
    {
        $this->scheduleService = $scheduleService;
        $this->gameService = $gameService;
        $this->resetService = $resetService;
    }

    public function generate(): JsonResponse
    {
        $this->scheduleService->schedule();
        return response()->json(['success' => true]);
    }

    public function getWeek(int $weekId): JsonResponse
    {
        return response()->json($this->gameService->getWeekDetails($weekId));
    }

    public function resetFixture(): JsonResponse
    {
        $this->resetService->reset();
        return response()->json(['success' => true]);
    }
}
