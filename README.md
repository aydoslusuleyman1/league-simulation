## LEAGUE SIMULATION SETUP

### YOU CAN INSTALL PROJECT BY FOLLOWING STEPS:


- ``docker-compose up -d --build``
- ``docker-compose run composer install``
- ``docker-compose run npm install``
- ``docker-compose run npm run watch``
- ``docker-compose run php php artisan migrate``
- ``docker-compose run php php artisan db:seed``
