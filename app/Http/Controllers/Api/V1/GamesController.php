<?php

namespace App\Http\Controllers\Api\V1;

use App\Domain\Game\Service\GameService;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GamesController extends Controller
{
    public function getGames(GameService $gameService): JsonResponse
    {
        return response()->json($gameService->getGames());
    }

    public function getWeekGames(GameService $gameService, int $weekId): JsonResponse
    {
        $weekGames = $gameService->getWeekDetails($weekId);
        return response()->json($weekGames);
    }
}
