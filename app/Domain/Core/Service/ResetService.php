<?php

namespace App\Domain\Core\Service;

use App\Domain\Game\Service\GameService;
use App\Domain\GameStatistics\Service\GameStatisticsService;
use App\Domain\Standing\Service\StandingService;

class ResetService
{
    private GameStatisticsService $gameStatisticsService;
    private GameService $gameService;
    private StandingService $standingService;

    public function __construct(GameStatisticsService $gameStatisticsService,
                                GameService $gameService,
                                StandingService $standingService
    ) {
        $this->gameStatisticsService = $gameStatisticsService;
        $this->gameService = $gameService;
        $this->standingService = $standingService;
    }

    public function reset(): bool
    {
        $this->gameStatisticsService->removeAllStatistics();
        $this->gameService->removeAllGames();
        $this->standingService->clearStandings();

        return true;
    }
}
