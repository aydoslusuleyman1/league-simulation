<?php

namespace App\Domain\Core\Enum;

final class GameResultEnum
{
    const WIN = 1;
    const DRAWN = 0;
    const LOST = 2;
}
