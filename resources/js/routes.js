const Home = () => import('./components/Home.vue')
const Fixture = () => import('./components/Fixture.vue')
const Simulation = () => import('./components/Simulation.vue')
export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'simulation',
        path: '/simulation',
        component: Simulation
    },
    {
        name: 'fixture',
        path: '/fixture',
        component: Fixture
    }
]
