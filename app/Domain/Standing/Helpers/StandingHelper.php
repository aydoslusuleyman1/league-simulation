<?php

namespace App\Domain\Standing\Helpers;

use App\Domain\Game\Service\GameService;

class StandingHelper
{
    public static function isLastWeek(int $weekId): bool
    {
        /** @var GameService $gameService */
        $gameService = app(GameService::class);
        $maxWeekIndex = $gameService->getMaxWeekIndex();

        return $weekId == $maxWeekIndex;
    }
}
