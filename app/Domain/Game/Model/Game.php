<?php

namespace App\Domain\Game\Model;

use App\Domain\GameStatistics\Model\GameStatistics;
use App\Domain\Team\Model\Team;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $table = 'games';
    protected $guarded = [];

    public function homeTeam()
    {
        return $this->belongsTo(Team::class,'home_team_id','id');
    }

    public function awayTeam()
    {
        return $this->belongsTo(Team::class,'away_team_id','id');
    }

    public function teamStatistics($teamId)
    {
        return $this->hasOne(GameStatistics::class,'game_id','id')->where('team_id', $teamId);
    }
}
