<?php

namespace App\Domain\Game\Service;

use App\Domain\Game\Repository\Contacts\GameRepositoryInterface;
use App\Domain\GameStatistics\Service\GameStatisticsService;
use App\Domain\Team\Model\Team;
use App\Domain\Team\Service\TeamService;
use App\Events\GameResultEvent;

class GameService
{
    private GameRepositoryInterface $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function saveGame(int $homeTeamId, int $awayTeamId, int $weekIndex): void
    {
        $this->gameRepository->saveGame($homeTeamId, $awayTeamId, $weekIndex);
    }

    public function getGames(): array
    {
        $games = $this->gameRepository->getGames();
        $response = [];
        foreach ($games as $game) {
            $response[$game->week][] = [
                'week' => $game->week,
                'home_team_id' => $game->homeTeam->id,
                'home_team_name' => $game->homeTeam->name,
                'away_team_id' => $game->awayTeam->id,
                'away_team_name' => $game->awayTeam->name,
            ];
        }

        return $response;
    }

    public function getWeekDetails(int $weekId, bool $getFurther = false): array
    {
        $weekData = $this->gameRepository->getGamesByWeekId($weekId, $getFurther);
        $response = [];
        foreach ($weekData as $game) {
            $response[] = [
                'id' => $game->id,
                'week' => $game->week,
                'home_team_id' => $game->homeTeam->id,
                'home_team_name' => $game->homeTeam->name,
                'home_team_statistics' => $game->teamStatistics($game->homeTeam->id)->first(),
                'away_team_id' => $game->awayTeam->id,
                'away_team_name' => $game->awayTeam->name,
                'away_team_statistics' => $game->teamStatistics($game->awayTeam->id)->first(),
            ];
        }

        return $response;
    }

    public function getMaxWeekIndex(): int
    {
        return $this->gameRepository->getMaxWeekIndex();
    }

    public function removeAllGames(): void
    {
        $allGames = $this->gameRepository->getAll();
        foreach ($allGames as $game) {
            $game->delete();
        }
    }

    public function simulateGames(int $weekId, bool $playAll)
    {
        $weekGames = $this->getWeekDetails($weekId, $playAll);

        /** @var GameStatisticsService $gameStatisticsService */
        $gameStatisticsService = app(GameStatisticsService::class);

        /** @var TeamService $teamService */
        $teamService = app(TeamService::class);

        foreach ($weekGames as $weekGame) {
            $isGamePlayed = $gameStatisticsService->isGamePlayed($weekGame['id']);
            if ($isGamePlayed) {
                continue;
            }

            $team1RedCard = $this->decideRedCard();
            $team2RedCard = $this->decideRedCard();

            $team1 = $teamService->getTeam($weekGame['home_team_id']);
            $team2 = $teamService->getTeam($weekGame['away_team_id']);

            $team1Score = $this->score($team1->squad_power, $team1RedCard);
            $team2Score = $this->score($team2->squad_power, $team1RedCard);

            $eventData = [
                'game_id' => $weekGame['id'],
                'home_team_id' => $weekGame['home_team_id'],
                'home_score' => $team1Score,
                'home_red_card' => $team1RedCard,
                'away_team_id' => $weekGame['away_team_id'],
                'away_score' => $team2Score,
                'away_red_card' => $team2RedCard,
            ];
            event(new GameResultEvent($eventData));
        }
    }

    public function score(int $squadPower, bool $redCardStatus): float
    {
        $maxGoal = 7;
        return round(rand(0, $maxGoal - $redCardStatus) * $squadPower / 100);
    }

    public function decideRedCard(): bool
    {
        return rand(0, 1);
    }
}
