<?php

namespace App\Domain\GameStatistics\Service\GameResultHandler;

use App\Domain\Core\Enum\GameResultEnum;
use App\Domain\GameStatistics\Helper\GameStatisticsHelper;
use App\Domain\GameStatistics\Repository\Contracts\GameStatisticsRepositoryInterface;
use App\Domain\Standing\Repository\Contacts\StandingRepositoryInterface;

class HomeTeamResultHandler implements GameResultHandlerInterface
{
    public function handle(array $data)
    {
        $gameStatisticsRepository = app(GameStatisticsRepositoryInterface::class);
        $status = GameStatisticsHelper::getMatchResult($data['home_score'], $data['away_score']);
        $homeTeamData = [
            'game_id' => $data['game_id'],
            'team_id' => $data['home_team_id'],
            'score' => $data['home_score'],
            'red_card' => $data['home_red_card'],
            'status' => $status
        ];

        $gameStatisticsRepository->insertGameStatisticByTeam($homeTeamData);

        $standingRepository = app(StandingRepositoryInterface::class);
        $homeTeamStandingData = $standingRepository->getTeamStanding($data['home_team_id']);

        $standingData = [
            'team_id' => $data['home_team_id'],
            'points' => $homeTeamStandingData->points + GameStatisticsHelper::getPointsByStatus($status),
            'goals_for' => $homeTeamStandingData->goals_for + $data['home_score'],
            'goals_against' => $homeTeamStandingData->goals_against + $data['away_score'],
            'win' => $status == GameResultEnum::WIN ? $homeTeamStandingData->win + 1 : $homeTeamStandingData->win,
            'drawn' => $status == GameResultEnum::DRAWN ? $homeTeamStandingData->drawn + 1 : $homeTeamStandingData->drawn,
            'lost' => $status == GameResultEnum::LOST ? $homeTeamStandingData->lost + 1 : $homeTeamStandingData->lost,
        ];

        $standingRepository->updateStandingByTeamId($standingData);
    }
}
