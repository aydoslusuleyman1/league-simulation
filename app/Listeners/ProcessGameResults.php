<?php

namespace App\Listeners;

use App\Domain\Core\Enum\TeamTypeEnum;
use App\Domain\GameStatistics\Service\GameResultHandler\GameResultHandler;
use App\Events\GameResultEvent;

class ProcessGameResults
{
    /**
     * Handle the event.
     *
     * @param \App\Events\GameResultEvent $event
     * @return void
     */
    public function handle(GameResultEvent $event)
    {
        (new GameResultHandler(TeamTypeEnum::HOME_TEAM))->init($event->data);
        (new GameResultHandler(TeamTypeEnum::AWAY_TEAM))->init($event->data);
    }
}
