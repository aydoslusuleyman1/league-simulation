<?php

namespace App\Domain\Team\Service;

use App\Domain\Team\Repository\Contacts\TeamRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class TeamService
{
    private TeamRepositoryInterface $teamRepository;

    public function __construct(TeamRepositoryInterface $teamRepository)
    {
        $this->teamRepository = $teamRepository;
    }

    public function getAllTeams(): Collection
    {
        return $this->teamRepository->getAllTeams();
    }

    public function getTeam(int $id)
    {
        return $this->teamRepository->getTeam($id);
    }
}
