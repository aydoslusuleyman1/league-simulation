<?php

namespace App\Domain\Team\Repository\Contacts;

interface TeamRepositoryInterface
{
    public function getTeam(int $id);
    public function getAllTeams();
}
